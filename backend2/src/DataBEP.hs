{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module DataBEP where
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics

-- Data model for BEP
-- e.g DataBEP { fixedCost = 500000000, variableCost = 1000000, marginRate = [10, 20] }
data DataBEP = DataBEP { fixedCost :: Float
                        , variableCost :: Float
                        , marginRate :: [Float]
                        , unitsPerMonth :: Int
                        }
                        deriving (Show, Generic)

-- encode and decode object to json
instance ToJSON DataBEP
instance FromJSON DataBEP