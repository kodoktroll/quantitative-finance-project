{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Response where
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics

-- Data model for Response
data Response = Response { fixedCostBEP :: Float
                         , variableCostBEP :: Float
                         , marginRateBEP :: [Float]
                         , unitsPerMonthBEP :: Int
                         , calculationType :: String
                         , result :: [Int]
                         }
                         deriving (Show, Generic)

-- encode and decode object to json
instance ToJSON Response
instance FromJSON Response