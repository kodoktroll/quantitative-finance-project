{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Formula where

-- validating integer for denominator division
validator a b 
    | a == b = False
    | otherwise = True

-- check bep type
beptype :: Int -> String
beptype unitsPerMonth
    | unitsPerMonth == 0 = "Units"
    | otherwise = "Months"

-- calculate bep units in months given input of margin rate
calculatebep :: Int -> Int -> [Int] -> Int -> [Int]
calculatebep fixedCost variableCost marginRate unitsPerMonth =
    map (\m -> bepmonths (bepunit fixedCost variableCost (calculateprice m variableCost)) unitsPerMonth) marginRate

-- calculate bep units of given input one margin rate
bepunit :: Int -> Int -> Int -> Int
bepunit fixedCost variableCost productPrice 
    | validator variableCost productPrice = fixedCost `div` (productPrice - variableCost)
    | otherwise = 0
    
-- calculate bep units in months given one margin rate
bepmonths :: Int -> Int -> Int
bepmonths bepUnit unitsPerMonth
    | unitsPerMonth == 0 = bepUnit
    | otherwise = ceiling $ fromIntegral bepUnit / fromIntegral unitsPerMonth

-- calculate product price of given input
calculateprice :: Int -> Int -> Int
calculateprice marginRate variableCost = ((marginRate * variableCost) `div` 100) + variableCost

-- calculate bep units in months given input of margin rate
calculatebepmonths :: Int -> Int -> [Int] -> Int -> [Int]
calculatebepmonths fixedCost variableCost marginRate unitsPerMonth =
    map (\m -> bepmonths (bepunit fixedCost variableCost (calculateprice m variableCost)) unitsPerMonth) marginRate

calculatebepmonthsfloat :: Float -> Float -> [Float] -> Int -> [Int]
calculatebepmonthsfloat fixedCost variableCost marginRate unitsPerMonth =
    map (\m -> bepmonths (bepunitfloat fixedCost variableCost (calculatepricefloat m variableCost)) unitsPerMonth) marginRate

calculatepricefloat :: Float -> Float -> Float
calculatepricefloat marginRate varCost = ((marginRate * varCost) / 100) + varCost

bepunitfloat :: Float -> Float -> Float -> Int
bepunitfloat fixedCost variableCost productPrice 
    | validator variableCost productPrice = ceiling (fixedCost / (productPrice - variableCost))
    | otherwise = 0

calculatebepfloat :: Float -> Float -> [Float] -> [Int]
calculatebepfloat fixedCost variableCost marginRate = map (\m -> bepunitfloat fixedCost variableCost (calculatepricefloat m variableCost)) marginRate
