{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where
import System.Environment
import Web.Scotty
import Network.HTTP.Types
import DataBEP
-- import DataBEPMonth
import Formula
import Response
-- import ResponseMonth

main = do
  port <- read <$> getEnv "PORT"
  scotty port $ do
  -- scotty 8080 $ do
  -- post data bep in units
  -- Assumption : fixedCost and variableCost already calculated from frontend
  --              marginRate is a list of Int of percentage e.g: 10% = 10 
  -- request body :
  -- {
  --     "fixedCost": 500000000,
  --     "variableCost": 1000000,
  --     "marginRate": [10, 20, 30],
  --     "unitsPerMonth": 0
  -- }
  -- Response in JSON
    post "/bep" $ do
      addHeader "Access-Control-Allow-Origin" "*"
      addHeader "Access-Control-Allow-Method" "POST"
      databep <- jsonData :: ActionM DataBEP                  -- Decode body of the POST request as an DataBEP object
      let fixedcost = fixedCost databep
      let variablecost = variableCost databep
      let marginrate = marginRate databep
      let unitspermonth = unitsPerMonth databep
      let calculationtype = beptype unitspermonth
      let bepresult = calculatebepmonthsfloat fixedcost variablecost marginrate unitspermonth
      -- Send the encoded object back as JSON
      json $ Response { fixedCostBEP = fixedcost
                      , variableCostBEP = variablecost
                      , marginRateBEP = marginrate
                      , unitsPerMonthBEP = unitspermonth
                      , calculationType = calculationtype
                      , result = bepresult
                      }