# Pemrograman Fungsional - REG - Gasal 2019/2020

## Team
- Aditya Yudha Pratama -1606917683
- Harlan Haidi - 1606877370
- Muhammad At Thoriq - 1606918484
- Muhammad Volyando Belvadra - 1606918326
- Tio Bagas Sulistiyanto - 1606918452

## Group Project - Break-Even Analysis
A business’s break-even point is the stage at which revenues equal costs. This project intends to provide a break-even calculation given a set of inputs([1](#v1.0)), and produce break-even point in units.

live demo: https://bep-calculator.herokuapp.com

#### v1.0
Mandatory Input:
- Variable Cost
- Fixed Cost
- Margin rate (in range) per product
- Unit sold per month

Output:
- Break-even point in units
- Break even point in months

<br />

# Backend

We use Scotty as a Haskell web framework.<br />
Scotty is the cheap and cheerful way to write RESTful, declarative web applications.

## Prerequisites

You need to install some software to get the backend running.

### Haskel platform

Can be found in the [Haskell Platform](https://www.haskell.org/platform/)

## How To Run on Local
First open your shell and navigate to `./backend2` directory. After that, export / set PORT variable on your shell.

If you're using Unix system, do this.
```bash
$ export PORT=5000
```
If you're using Windows system, do this.
```powershell
> SET PORT=5000
```
After that, run the project by this command.
```bash
$ cabal new-run
```
Done! the backend will run on `localhost:PORT` you've specified.


## API

POST http://bep-calculator-backend.herokuapp.com/bep

input: 

```json
{
    "fixedCost"    : float,
    "variableCost" : float,
    "marginRate"   : [float],
    "unitsPerMonth": int (leave it to 0 if you only want break even point in units)
}
```

output: 

```json
{
    "fixedCostBEP"    : float,
    "variableCostBEP" : float, 
    "marginRateBEP"   : [float],
    "calculationType" : String ("Units" if unitsPerMonth = 0 or "Months" otherwise),
    "result" : [int (in months if calculationType = "Months" and in units otherwise)]
}
```

## More Information

Read [Backend README.md](https://gitlab.com/kodoktroll/quantitative-finance-project/tree/master/backend)

Tutorials and related projects can be found in the [Scotty wiki](https://github.com/scotty-web/scotty/wiki).

<br>

# Frontend

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How To Run on Local

Run `npm install` in the `frontend/` directory to ensure all depedencies are downloaded.

Before you run the project, please specify Backend Endpoint on `frontend/src/actions/Actions.ts`. If you're going to use local Backend server specify `http://localhost:PORT`.

In frontend directory, you can run:
```bash
$ npm run start
```

Read [Frontend README.md](https://gitlab.com/kodoktroll/quantitative-finance-project/tree/master/frontend) to read more information.
