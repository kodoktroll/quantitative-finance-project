import React from 'react';

import Form from '../src/components/Form';
import Field from '../src/components/Field';
import '../src/index.css'

export default {
    title: 'Form BEP',
};

export const FormBEP = () => <Form />
export const FieldBEP = () => <Field />