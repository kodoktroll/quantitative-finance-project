export const CALCULATE_BEP = "CALCULATING_BEP";
export const CALCULATE_SUCCESS = "CALCULATION_SUCCESS";
export const CALCULATE_FAILED = "CALCULATION_FAILED";
export const WAIT_STATUS = "Is processing...";
export const SUCCESS_STATUS = "Done processing";