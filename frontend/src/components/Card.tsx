import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import theme from './css/materialuicss';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1,1),
    borderRadius: '30px',
    fontFamily: 'Noto Sans JP',
  },
  typography: {
    fontFamily: 'Noto Sans JP',
  }
}));

interface IFieldProps {
  margin: number,
  bep: number,
  calculationType?: string,
}

const Card: React.FC<IFieldProps> = (
  { margin, bep, calculationType }
) => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme} >
      <Paper className={classes.root}>
      <Typography variant="body2">
      {`With margin ${margin} %`}
      </Typography>
      <Typography variant="body1">
        Brek-even in <b> {`${bep} ${calculationType}`} </b>
      </Typography>
    </Paper>
    </ThemeProvider>
    
  );
}

export default Card;