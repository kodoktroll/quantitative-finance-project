import React from 'react';

import './css/field.css';

interface IFieldProps {
    key: string | number,
    id: string,
    section: string,
    name: string[],
    labelOne: string,
    labelTwo: string,
    valueOne: string | number,
    valueTwo: number,
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
}

const Field: React.FC<IFieldProps> = 
    ({key, id, section, name, labelOne, labelTwo, valueOne, valueTwo, handleChange}) => {
    return (
        <div className="each-field" key={key}>
            <div className="label-input">
                <p className="label-block">{labelOne}</p>
                { 
                    ( name[0] === "component") ?
                    <input
                        id={id}
                        type="text"
                        className={"input-block "+section}
                        name={name[0]}
                        value={valueOne}
                        onChange = {handleChange}
                    /> : 
                    <input
                        id={id}
                        type="number"
                        className={"input-block "+section}
                        name={name[0]}
                        value={valueOne}
                        onChange = {handleChange}
                        pattern="[0-9]{10}"
                        step="10"
                        min="0"
                    />
                }
            </div>
            {
                (name[0] !== "unit") ?
                    <div className="label-input">
                        <p className="label-block">{labelTwo}</p>
                        <input
                            id={id}
                            type="number"
                            className={"input-block "+section}
                            name={name[1]}
                            value={valueTwo}
                            onChange = {handleChange} 
                            pattern="[0-9]{10}"
                            step="10"
                            min="0"
                        />
                    </div>
                    :null
            }

            
        </div>
    )
}

export default Field;