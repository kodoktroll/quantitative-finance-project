import React, { ReactElement } from 'react';
import {connect} from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { Typography, Divider, Paper, Box } from '@material-ui/core';

import { BEPState } from '../actions/ActionType'
import Card from './Card'

import theme from './css/materialuicss';

const mapStateToProps = (state: any) => {
  return { data: state.CalculationBEP };
  
}

interface IContainerProps {
  data: BEPState,
}

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
    borderRadius: '30px',
    marginLeft: '100px',
  },
  bepList: {
    display: 'flex',
  }
}));

const Container: React.FC<IContainerProps> = ({data}) => {
  const classes = useStyles();

  const card = (key: number, idx: number): ReactElement => {
    return (
      <Card 
        key={`${key} ${idx}`}
        margin={data.marginRate[idx]}
        bep={key}
        calculationType={data.calculationType}
      />
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <Paper className={`${classes.root}`}>
      <Typography variant="h2" component="h2">
        Calculation Result
      </Typography>
      <br />
      <Divider />
      <br />


      {data.result ? (
          <div>
            <Box m={1.5}>
              <Typography variant="body1">
                <b>{`Fixed Cost: $${data.fixedCost}`}</b>
              </Typography>
              <Typography variant="body1">
                <b>{`Variable Cost: $${data.variableCost}`}</b>
              </Typography>
              <Divider />
              <Typography variant="body1">
                <b>{`Total Cost: $${data.fixedCost + data.variableCost}`}</b>
              </Typography>
            </Box>
            <Box
              display="flex"
              flexWrap="wrap"
            >
            {data.result.map(card)}
          </Box>
        </div>
      ): (
        <Typography variant="h6" component="p">
          No result
        </Typography>
      )}
      
    </Paper>
    </ThemeProvider>
    
  );
}

const List = connect(mapStateToProps)(Container);
export default List;