import { createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        fontFamily: 'Noto Sans JP',
        body1: {
            color: '#1b2a49',
        },
        body2: {
            fontSize: '12px',
        },
        h2: {
            // fontFamily: 'Noto Sans JP',
            fontSize: '24px',
            color: '#1b2a49',
            fontWeight: 'bolder',
        },
        h6: {
            // fontFamily: 'Noto Sans JP',
            fontSize: '15px',
            color: 'grey',
        }
    }
  })

export default theme;