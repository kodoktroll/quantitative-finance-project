import React, { useState, ReactElement } from 'react';

import Field from './Field';
import myuuid4 from '../lib/uid';

import {connect} from 'react-redux';
import {calculateBEP} from '../actions/Actions';
import {BEPAction, BEPPayload} from '../actions/ActionType';

import './css/form.css';

type State = "fixedCost" | "variableCost" | "margin";

interface IFormProps {
    kirim: (payload: BEPPayload) => BEPAction,
}

const Form: React.FC<IFormProps> = ({kirim}) => {

    type CostComponent = {
        id: string,
        component: string,
        price: number,
    }

    const [state, setState] = useState({
        fixedCost: [{id: myuuid4(), component: "", price: 0}],
        variableCost:  [{id: myuuid4(), component: "", price: 0}],
        margin: [0,0],
        unit: 0,
    })

    const getPrice = (cost: {id: String, component: String, price: number}): number => {
        return cost['price'];
    }

    const add = (number1: number, number2: number): number => {
        return number1 + number2;
    }

    const handleSubmit = (): void => {
        var total_fixed_cost = state.fixedCost
                                .map(getPrice)
                                .reduce(add);
        
        var total_variable_cost = state.variableCost
                                .map(getPrice)
                                .reduce(add);
        
        var list_margin = Array(state.margin[1] - state.margin[0] + 1)
                            .fill(state.margin[0])
                            .map(add);

        
        var payload = {
            fixedCost: total_fixed_cost,
            variableCost: total_variable_cost,
            marginRate: list_margin,
            unitsPerMonth: state.unit,
        }
        removeForm();
        kirim(payload);

    }

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        if (e.target.className.includes("margin")) {
            const key: number = Number(e.target.name);
            let margins = [...state.margin];
            margins[key] = Number(e.target.value);
            setState({
                ...state,
                margin: margins
            })
        }
        else if (e.target.className.includes("unit")){
            setState({
                ...state,
                unit:  Number(e.target.value),
            })
        }
        else {
            let costs = [];
            let name_cost = "";
            if (e.target.className.includes("fixedCost")) {
                costs = [...state.fixedCost];
                name_cost = "fixedCost"
            }
            else {
                costs = [...state.variableCost];
                name_cost = "variableCost"
            }
            const key: string = e.target.name;
            if (key.includes("component")) {
                costs[Number(e.target.id)].component = e.target.value;
            }
            else if (key.includes("price")) {
                costs[Number(e.target.id)].price = Number(e.target.value);
            }
            setState({
                ...state,
                [name_cost]: costs,
            })
        }
    }

    const handleAddInput = (e: React.MouseEvent): void => {
        const key: State = e.currentTarget.id as State;
        setState({
            ...state,
            [e.currentTarget.id]: [...state[key], {id: myuuid4(), component:"", price: 0}]
        })
    }

    const removeForm = (): void => {
        setState({
            fixedCost: [{id: myuuid4(), component: "", price: 0}],
            variableCost:  [{id: myuuid4(), component: "", price: 0}],
            margin: [0,0],
            unit: 0,
        })
    }

    const fixedCostField = (val: CostComponent, idx: number): ReactElement => {
        return (
            <Field 
                key={val.id}
                section="fixedCost"
                name={["component", "price"]}
                id={""+idx}
                labelOne="Component" 
                labelTwo="Price" 
                valueOne={val.component}
                valueTwo={val.price}
                handleChange={handleChange}
            />
        );
    }

    const variableCostField = (val: CostComponent, idx: number): ReactElement => {
        return (
            <Field
                key={val.id}
                section="variableCost"
                name={["component", "price"]}
                id={""+idx}
                labelOne="Component" 
                labelTwo="Price" 
                valueOne={val.component}
                valueTwo={val.price}
                handleChange={handleChange}
            />
        );
    }

    return (
        <form>
            <h2 id="form-title">Input Your Costs</h2>
            <div className="field iterate-field">
                <h3 className="field-title fixed">Fixed Cost</h3>
                {
                    state.fixedCost.map(fixedCostField)
                }
                <p className="addinput" id="fixedCost" onClick={handleAddInput}>Add Input</p>
            </div>
            <div className="field iterate-field" >
                <h3 className="field-title var">Variable Cost</h3>
                {
                    state.variableCost.map(variableCostField)
                }
                <p className="addinput" id="variableCost" onClick={handleAddInput}>Add Input</p>
            </div>
            <div className="field">
                <h3 className="field-title margin">Margin</h3>
                <Field
                    key="margin"
                    section="margin"
                    name={["0", "1"]}
                    id="margin"
                    labelOne="Start" 
                    labelTwo="End" 
                    valueOne={state.margin[0]}
                    valueTwo={state.margin[1]}
                    handleChange={handleChange}
                />
            </div>
            <div className="field">
                <h3 className="field-title unit">Unit Sold Per Month</h3>
                <Field
                    key="unit"
                    section="unit"
                    name={["unit"]}
                    id="unit"
                    labelOne="unit" 
                    labelTwo="End" 
                    valueOne={state.unit}
                    valueTwo={state.margin[1]}
                    handleChange={handleChange}
                />
            </div>
            <button className="submit" type="button" onClick={handleSubmit}>
                Submit
            </button>
        </form>
    )
}



//  var dispatch = useDispatch;
const mapDispatchToProps = (dispatch: any) => {
    return ({
        kirim : (a: BEPPayload) => dispatch(calculateBEP(a)),
    });
}

const form2 = connect(null, mapDispatchToProps)(Form);
export default form2;