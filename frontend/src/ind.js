import store from "./store/Store";
import { calculateBEP } from "./actions/Actions";

window.store = store;
window.calculateBEP = calculateBEP;