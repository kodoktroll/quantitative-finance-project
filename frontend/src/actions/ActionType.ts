export interface BEPPayload {
    fixedCost: number,
    variableCost: number,
    marginRate: number[],
    unitsPerMonth?: number,
}

export interface ICalculateBEP {
    type: String,
    payload: BEPPayload,
    result?: number[],
    status: string,
    calculationType?: string,
}

export interface IBEPState extends BEPPayload {
    result?: number[],
    status?: string,
    calculationType?: string,
}

export type BEPAction = ICalculateBEP
export type BEPState = IBEPState