import axios from 'axios';
import {CALCULATE_BEP, CALCULATE_SUCCESS, CALCULATE_FAILED, WAIT_STATUS, SUCCESS_STATUS} from '../constants/ActionType'
import {BEPPayload, BEPAction} from './ActionType'

const ENDPOINT = "http://bep-calculator-backend.herokuapp.com/bep";

export function calculateBEP
    (payload: BEPPayload): any {
    // return { type: CALCULATE_BEP, payload}
    return (dispatch: any) => {
        dispatch(calculatingBEP(payload));
        
        axios
            .post(ENDPOINT, {
                "fixedCost": payload.fixedCost,
                "variableCost": payload.variableCost,
                "marginRate": payload.marginRate,
                "unitsPerMonth": payload.unitsPerMonth,
            }, {
                headers: {
                    "Content-Type": "text/plain",
                }
            })
            .then((res:any) => {
                const payload: BEPPayload = {
                    fixedCost: res.data.fixedCostBEP,
                    variableCost: res.data.variableCostBEP,
                    marginRate: res.data.marginRateBEP,
                    unitsPerMonth: res.data.unitsPerMonthBEP,
                }
                console.log(res.data);
                dispatch(calculateBEPSuccess(payload, res.data.result, res.data.calculationType));
            })
            .catch((err:any) => {
                dispatch(calculateBEPFailed(payload, err.message));
            })
         
    }
};

const calculatingBEP = (payload: BEPPayload): BEPAction => {
    return {
        type: CALCULATE_BEP,
        payload: payload,
        status: WAIT_STATUS,
    }
};

const calculateBEPSuccess = (payload: BEPPayload, res: number[], type: string): BEPAction => ({
    type: CALCULATE_SUCCESS,
    payload: payload,
    result: res,
    status: SUCCESS_STATUS,
    calculationType: type,
})

const calculateBEPFailed = (payload: BEPPayload, message: string): BEPAction => ({
    type: CALCULATE_FAILED,
    payload: payload,
    status: message,
})