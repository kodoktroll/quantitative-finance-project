import React  from 'react';

import List from '../components/Container';
import Form from '../components/Form';
import './css/home.css';

const Home: React.FC = () => {
    return (
        <div className="home">
            <h1 id="title">Break-even Calculator</h1>
            <p id="caption">We will help you visualize your break-even product.</p>
            <div className="main-row">
                <div className="main-col form">
                    <Form />
                </div>
                <div className="main-col bep-results">
                    <List/>
                </div>
            </div>
        </div>
    )
}

export default Home;