import {BEPAction, BEPState} from '../actions/ActionType';
import { CALCULATE_BEP, CALCULATE_SUCCESS, CALCULATE_FAILED } from '../constants/ActionType';


function BEPReducer(
    state: BEPState = { fixedCost: 0, variableCost: 0, marginRate: []}, 
    action: BEPAction): BEPState {
    
    if (action.type === CALCULATE_BEP) {
        return Object.assign({}, state, {
            status: action.status,
        });
    }
    if (action.type  === CALCULATE_SUCCESS) {
        return Object.assign({}, state, {
            fixedCost: action.payload.fixedCost,
            variableCost: action.payload.variableCost,
            marginRate: action.payload.marginRate,
            result: action.result,
            status: action.status,
            calculationType: action.calculationType,
        });
    }
    if (action.type  === CALCULATE_FAILED) {
        return Object.assign({}, state, {
            status: action.status,
        });
    }
    return state;
}

export default BEPReducer;