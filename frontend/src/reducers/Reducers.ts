import { combineReducers } from 'redux';
import BEPReducer from './BEPReducer';

export default combineReducers({
    CalculationBEP : BEPReducer
})